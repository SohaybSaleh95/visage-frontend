import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drug-product-setup',
  templateUrl: './drug-product-setup.component.html',
  styleUrls: ['./drug-product-setup.component.css']
})
export class DrugProductSetupComponent implements OnInit {

  options:any = [1, 2, 3];
  constructor() {
    this.options = [
      { label: 'Select City', value: null },
      { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
      { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
      { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
      { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
      { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } }
    ];
  }

  ngOnInit() {
  }

}
