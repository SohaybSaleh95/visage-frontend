import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrugProductSetupComponent } from './drug-product-setup.component';

describe('DrugProductSetupComponent', () => {
  let component: DrugProductSetupComponent;
  let fixture: ComponentFixture<DrugProductSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugProductSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugProductSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
