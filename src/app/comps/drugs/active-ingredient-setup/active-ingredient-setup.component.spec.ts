import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveIngredientSetupComponent } from './active-ingredient-setup.component';

describe('ActiveIngredientSetupComponent', () => {
  let component: ActiveIngredientSetupComponent;
  let fixture: ComponentFixture<ActiveIngredientSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveIngredientSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveIngredientSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
